﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace searchEngine
{
    class Program
    {
        static void Main(string[] args)
        {
            
            String path = @"C:\Users\USER\Desktop\fruits.txt";
            string file = File.ReadAllText(path);
            Console.Write("please enter a word to search for in the file : ");
            string word = Console.ReadLine();
            TextReader myfile = File.OpenText(path);
            string line = myfile.ReadLine();
            int position = line.IndexOf(word);
            int count = 0; 
            
            while (line != null)
            {
                if (position != -1)
                {
                    count++;
                }
                line = myfile.ReadLine();
            }
            if (count == 0)
            {
                Console.WriteLine("your word was not found!");
            }
            else
            {
                Console.WriteLine("Your word was found ");
            }
            Console.ReadLine();
        }
    }
}